# Arga11y - Job template 

This repository provides a GitLab job template to incorporate Arga11y into your web app's GitLab pipeline. Arga11y is a tool that helps ensure compliance with Argentine accessibility regulations. This integration can be achieved by including the job template in your GitLab CI/CD configuration. This repository also includes a .pa11yci.json configuration file that serves as an example of how to configure your settings to ensure that only the accessibility criteria from the WCAG 2.0 standard are included
